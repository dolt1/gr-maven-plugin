# html-demo

#### 项目介绍
**项目简介**

gr-maven-plugin 整理代码注释的插件

#### 软件架构
maven plugin


#### 安装教程

1. 添加插件

	<plugin>
	    <groupId>com.mook.plugin</groupId>
	    <artifactId>gr-maven-plugin</artifactId>
	    <version>0.0.1-SNAPSHOT</version>
	    <configuration>
	    </configuration>
	</plugin>


#### 使用说明

1. 添加依赖

	<dependency>
		<groupId>com.mook.plugin</groupId>
		<artifactId>gr-maven-plugin</artifactId>
		<version>0.0.1-SNAPSHOT</version>
	</dependency>

2. 项目根目录添加comment.txt
3. 执行
mvn gr:drive

#### 参与贡献

[Guide java plugin development](https://maven.apache.org/guides/plugin/guide-java-plugin-development.html)