package com.mook.plugin.gr;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.regex.Matcher;

public class Remover {

	public void removeIt(String dist) throws IOException {
		File srcDir = new File("src");
		File outDir = new File(dist);

		if (!srcDir.exists()) {
			return;
		}
		if (!outDir.exists()) {
			outDir.mkdirs();
		}

		convert(srcDir, outDir);

	}

	void convert(File srcDir, File dist) throws IOException {
		if (!srcDir.exists()) {
			return;
		}
		File[] list = srcDir.listFiles(new FilenameFilter() {

			public boolean accept(File dir, String name) {
				// TODO Auto-generated method stub
				return name.endsWith(".java") || new File(dir, name).isDirectory();
			}

		});

		for (File f : list) {
			System.out.println("canon: " + f.getPath());
			if (f.isDirectory()) {
				convert(f, dist);
			} else {
				File outFile = new File(dist, f.getPath());
				if (!outFile.getParentFile().exists()) {
					outFile.getParentFile().mkdirs();
				}
				if (!outFile.exists()) {
					outFile.createNewFile();
				}
				removeComment(f, outFile);
			}
		}
	}

	void removeComment(File srcFile, File outFile) throws IOException {
		// TODO Auto-generated method stub
		// File srcFile = new File("src/Remover.java");
		// File outFile = new File("Remover1.java");
		// System.out.println(srcFile.getAbsolutePath());
		// FileOutputStream fout = new FileOutputStream(outFile);
		BufferedReader fr = new BufferedReader(new FileReader(srcFile));
		BufferedWriter fw = new BufferedWriter(new FileWriter(outFile));
		StringBuffer sbuf = new StringBuffer();
		String sline = null;
		do {
			sline = fr.readLine();
			if (sline != null) {
				String noCommentLine = removeLineComment(sline, 0);
				if (noCommentLine != null) {
					sbuf.append(noCommentLine + "\n");
				}
			}
		} while (sline != null);
		String str = removeBlockComment(sbuf.toString());

		String comment = getHeadComment();
		fw.write(comment);
		fw.write(str);
		fw.flush();
		fw.close();
		fr.close();
	}

	/**
	 * 祛除行注释
	 */
	String removeLineComment(String lineStr, int start) {
		if (lineStr.substring(start).contains("//")) {
			int suspectIndex = lineStr.indexOf("//", start);
			String prefix = lineStr.substring(0, suspectIndex);
			int quotesNum = prefix.length() - prefix.replaceAll("\"", "").length();
			System.out.println(prefix + "#" + suspectIndex + " : " + quotesNum);

			if (prefix.contains("\"") && quotesNum % 2 == 1) {
				// 有奇数个引号，行注释符在字符串内。
				System.err.println(lineStr);
				return removeLineComment(lineStr, suspectIndex + 1);
			} else {
				String line = lineStr.substring(0, suspectIndex);
				if (line.trim().length() == 0) {
					return null;
				} else {
					return line;
				}
			}
		} else {
			return lineStr;
		}
	}

	/**
	 * /* *
	 */
	String removeBlockComment(String str) {
		// /*()*/
		String regex = "(?s)/\\*(.(?!\\*/)){1,}.(?:\\*/)";
		// // java.util.regex.Pattern.compile(regex).matcher(str).replaceAll(repl) ;
		Matcher matcher = java.util.regex.Pattern.compile(regex).matcher(str);
		while (matcher.find()) {
			// System.out.println("[" + matcher.group() + "]");
		}
		// str.rep
		// System.out.println(java.util.regex.Pattern.compile(regex,Pattern.DOTALL).toString());
		return str.replaceAll(regex, "");
	}

	String getHeadComment() throws IOException {
		File commentFile = new File("comment.txt");
		// System.out.println(commentFile.getAbsolutePath());
		// FileOutputStream fout = new FileOutputStream(outFile);
		BufferedReader fr = new BufferedReader(new FileReader(commentFile));
		StringBuffer sbuf = new StringBuffer();
		String sline = null;
		do {
			sline = fr.readLine();
			if (sline != null) {
				sbuf.append(sline + "\n");
			}
		} while (sline != null);
		fr.close();
		return sbuf.toString();
	}

}
