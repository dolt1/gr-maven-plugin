package com.mook.plugin.gr;

import java.io.IOException;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

@Mojo(name = "drive")
public class Car extends AbstractMojo {
	@Parameter(defaultValue = "8080")
	private Integer port;
	
	@Parameter(defaultValue = "gen-src")
	private String dist;

	public void execute() throws MojoExecutionException, MojoFailureException {
		System.out.println("Car drive...");
		System.out.println(port);
		System.out.println(dist);
		try {
			new Remover().removeIt(dist);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
